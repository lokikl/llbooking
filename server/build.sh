#!/usr/bin/env bash
set -e

if [[ ! -f Gemfile ]]; then
  echo "Please execute this script in the root directory"
  exit 1
fi

rm -rf public/extension.zip
zip -rj public/extension.zip ../client

docker build -t lokikl/llbooking .

docker push lokikl/llbooking

ssh pf1 "sudo bash -c 'cd /root/llbooking; ./refresh.sh'"
