FROM ubuntu:14.04
MAINTAINER Loki Ng "dev@lokikl.com"

ENV DEBIAN_FRONTEND noninteractive

# Install dependencies
RUN \
  apt-get update && \
  apt-get install -y build-essential openssl libffi-dev libgdbm-dev \
    libncurses5-dev libreadline-dev libssl-dev libyaml-dev zlib1g-dev \
    libxslt-dev wget curl git && \
  rm -rf /var/lib/apt/lists/*

# download source and compile
WORKDIR /src
RUN curl http://cache.ruby-lang.org/pub/ruby/2.2/ruby-2.2.2.tar.gz | tar -zx && \
  cd /src/ruby-2.2.2 && \
  ./configure --disable-install-doc && \
  make && \
  make install && \
  rm -rf /src/*

RUN gem install bundler thin parallel pry yajl-ruby excon redis nokogiri pry-stack_explorer pry-nav rspec --no-ri --no-rdoc

# https://docs.docker.com/compose/rails/
# checkout Gemfile and bundle first is to avoid bundle install again next time when any files in source code changed
# cuz cmd will be re-run if any files in the ADD command changed
RUN mkdir -p /bundle /llbooking
WORKDIR /llbooking
ADD Gemfile /llbooking/Gemfile
ADD Gemfile.lock /llbooking/Gemfile.lock
RUN bundle install --deployment --without development test --path /bundle

# change time zone to HK
RUN echo Asia/Hong_Kong > /etc/timezone && \
  dpkg-reconfigure --frontend noninteractive tzdata

RUN apt-get update -q 2 && apt-get install -qqy tesseract-ocr imagemagick --fix-missing


ADD ./ /llbooking

# https://github.com/docker/docker/issues/4032
RUN unset DEBIAN_FRONTEND
CMD ["/bin/bash"]
