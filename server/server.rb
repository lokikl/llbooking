require 'sinatra/base'
require 'redis'
require 'base64'

class BookingServer < Sinatra::Base
  set :root, File.dirname(__FILE__)

  configure do
    set :erb, :trim => '-'
  end

  helpers do
    def redis
      redis_url = ENV['REDISURL'] || 'redis://127.0.0.1:6379'
      info = {url: redis_url}
      if ENV['REDISAUTH']
        info[:password] = ENV['REDISAUTH']
      end
      @redis ||= Redis.new(info)
    end

    def erb_out(layout: 'main', page:, title: nil, locals: {})
      response.headers["Content-Type"] = "text/html"
      erb :"#{layout}/#{page}", :layout => :"#{layout}", :locals => {
        page_title: title || "Untitled page #{page}", layout: layout,
      }.merge(locals)
    end

  end

  post '/new/?' do
    config = params['config']
    code = config.fetch('code')
    if code[/[a-zA-Z0-9-]+/] != code
      erb_out(page: :new, locals: { config: config, msg: "Invalid Code, code should contains only alphanumberic characters and stash (-)." })
    else
      redis.mapped_hmset("llbooking:#{code}", config)
      redirect '/'
    end
  end

  get '/?' do
    if params['delete']
      code = params['delete']
      redis.del("llbooking:#{code}")
    end
    configs = redis.keys("llbooking:*").map { |key|
      key.split(':')[1..-1].join(':')
    }.sort
    erb_out(page: :index, locals: { configs: configs })
  end

  get '/new/?' do
    erb_out(page: :new)
  end

  get '/edit/:code/?' do |code|
    config = redis.hgetall("llbooking:#{code}")
    if config == {}
      redirect '/'
    else
      erb_out(page: :new, locals: { config: config, edit: true })
    end
  end


  get '/config/:code/select.js' do |code|
    headers "Content-Type" => "application/javascript; charset=utf8"
    config = redis.hgetall("llbooking:#{code}")
    if config == {}
      'Code not found'
    else
      # facilityHint, typeHint, timeHint, areaHint, venueHint
      options = config['options'].lines.map { |l|
        type, area, venue, time, count = l.strip.split(/ *, */)
        s, e = time.split('-').map(&:to_i)
        if s < 12
          time_range = 'Morning'
          slots = [*s..e-1].map { |i| i - 7 }
        elsif s < 18
          time_range = 'Afternoon'
          slots = [*s..e-1].map { |i| i - 12 }
        else
          time_range = 'Evening'
          slots = [*s..e-1].map { |i| i - 18 }
        end
        [config['facility'], type, time_range, area, venue, slots]
      }
      erb :'select.js', :locals => { config: config, options: options }
    end
  end

  get '/captcha.js' do
    headers "Content-Type" => "application/javascript; charset=utf8"
    FileUtils.mkdir_p('workspace')
    file = 'workspace/work.png'
    File.write(file, params['i'])
    system("convert inline:#{file} #{file}")
    # remove bg
    file2 = 'workspace/work2.png'
    system("composite -compose difference workspace/challenge-bg.png #{file} #{file2}")
    # Ignore minor differences (noise from bg)
    file3 = 'workspace/work3.png'
    system("convert -threshold 0% -fill white +opaque black #{file2} #{file3}")
    # used new challenge-bg
    # remove squares, seems not good enough
    # loop
    #   compare -metric rmse -subimage-search work3.png subimg.png result.png
    #   convert work3.png subimg-new.png -geometry +103+43 -compose over -composite work3.png
    # OCR
    output = `tesseract #{file3} stdout -psm 8 esd`.strip
    output = 'unknown' if output == ''
    output = 'unknown' if output.length != 6
    puts "output: #{output}"

    erb :'captcha.js', :locals => { code: output }
  end
end
