#!/usr/bin/env bash

thin start -p 4002 --ssl --ssl-key-file ssl/$1.key --ssl-cert-file ssl/$1.crt
