# http://blog.avirtualhome.com/adding-ssl-certificates-to-google-chrome-linux-ubuntu/
if ! which certutil > /dev/null 2>&1; then
  echo "installing certutil"
  sudo apt-get update
  sudo apt-get install libnss3-tools
fi
certutil -d sql:$HOME/.pki/nssdb -A -t P -n localhost.com -i localhost.com.pem
certutil -d sql:$HOME/.pki/nssdb -A -t P -n labpf.com -i labpf.com.pem
echo "done"
