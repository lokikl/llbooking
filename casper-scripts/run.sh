#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

sudo docker run --rm -v `pwd`:/mnt/test \
  rdpanek/casperjs:1.1-beta3 \
  /usr/bin/casperjs --ssl-protocol=tlsv1 --ignore-ssl-errors=true \
  /mnt/test/test.js
  #--cookies-file=/mnt/test/cookies.txt \

# dn rdpanek/casperjs:1.1-beta3 test

# sudo docker run --rm -v `pwd`:/mnt/test \
#   rdpanek/casperjs:1.1-beta3 \
#   /usr/bin/casperjs /mnt/test/login.js --cookies-file=/mnt/test/cookies.txt
