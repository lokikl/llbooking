var casper = require('casper').create({
  verbose: true,
  logLevel: 'debug',
	viewportSize: {
		width: 1280,
		height: 960
	},
	pageSettings: {
		loadImages: true,
		loadPlugins: false
	}
});

var url = 'http://w2.leisurelink.lcsd.gov.hk/leisurelink/application/checkCode.do?flowId=1&lang=EN';

casper.start(url, function() {
  this.echo(this.getTitle());
});

var captchaSelector = '.formPanel .cell img';
casper.then(function() {
  this.captureSelector('/mnt/test/captcha.png', captchaSelector);
});

var fs = require("fs");
var passFile = '/mnt/test/captcha.txt';

casper.then(function() {
  this.waitFor(function check() {
    return fs.exists(passFile);
  }, function then() {
    var code = fs.read(passFile);
    this.echo('Captcha input: ' + code);
    this.sendKeys(captchaSelector, code);
  }, function timeout() {
    this.echo('Timed out');
  }, 100000000000);
});

var url2 = 'https://t2.leisurelink.lcsd.gov.hk/lcsd/leisurelink/facilityBooking/login/login.jsp';
var x = require('casper').selectXPath;

casper.thenOpen(url2, function() {
  casper.waitForSelector("#hkId", function then() {
    this.captureSelector('/mnt/test/captcha.png', 'body');

    this.fill('form[name=LoginForm]', {
      'hkId': 'y031017',
      'hkIdCheckDigit': '2',
      'dobDD': '09',
      'dobMM': '05',
      'dobYYYY': '1989',
    }, true);

  }, function timeout() {
    this.captureSelector('/mnt/test/captcha.png', 'body');
  }, 100000000000);
});

casper.waitForSelector("select.gwt-ListBox", function then() {
  casper.evaluate(function(username, password) {
    document.querySelector('#datePanel select.gwt-ListBox').value = '20151106';
    document.querySelector('#facilityPanel select.gwt-ListBox').value = '7';
    $('#facilityPanel select.gwt-ListBox').trigger('change');
    // document.querySelector('#submit').click();
  }, 'sheldon.cooper', 'b4z1ng4');

  // this.fillSelectors('#tableLayout', {
  //   '#datePanel select.gwt-ListBox':       '20151106',
  //   '#facilityPanel select.gwt-ListBox':   '7',
  //   '#facilityTypePanel select.gwt-ListBox':   '504',
  //   '#sessionTimePanel select.gwt-ListBox':   'EV', // AM, PM, EV
  //   '#areaPanel select.gwt-ListBox':   'YTM' // AM, PM, EV
  // });
  this.captureSelector('/mnt/test/captcha.png', 'body');
});

casper.run();
