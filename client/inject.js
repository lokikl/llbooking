var $main = $('frame[name=main]');
var $captchaImg = $('form[name=CheckCodeForm] img');

// Captcha automation, WIP
// Code taken from MatthewCrumley (http://stackoverflow.com/a/934925/298479)
// http://w2.leisurelink.lcsd.gov.hk/leisurelink/image?a=0.49655152759840915
// generate url in ruby: '0.' + rand(1..99999999999999999).to_s.rjust(17, '0')
function getBase64Image(img) {
  // Create an empty canvas element
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;

  // Copy the image contents to the canvas
  var ctx = canvas.getContext("2d");
  ctx.drawImage(img, 0, 0);

  // Get the data-URL formatted image
  // Firefox supports PNG and JPEG. You could check img.src to guess the
  // original format, but be aware the using "image/jpg" will re-encode the image.
  var dataURL = canvas.toDataURL("image/png");
  img.src = dataURL;

  return dataURL;
}

function injectJqThenScript(scriptUrl) {
  var jq = document.createElement('script');
  // jq.src = '//ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js';
  jq.src = 'https://code.jquery.com/jquery-2.1.4.min.js';
  jq.onload = function() {
    var s = document.createElement('script');
    s.src = scriptUrl;

    s.onload = function() {
      this.parentNode.removeChild(this);
    };
    document.head.appendChild(s);
  };
  document.head.appendChild(jq);
}

// not in any expected page
if ($captchaImg.length > 0) {
  var img = getBase64Image($captchaImg[0]);
  console.log("Image length: " + img.length + ", injecting solver");
  img = encodeURIComponent(img);
  chrome.storage.sync.get({
    serverEndpoint: '',
  }, function(items) {
    var captchaJs = items.serverEndpoint + "/captcha.js?i=";
    injectJqThenScript(captchaJs + img);
  });

} else if ($main.length === 0) {
  console.log('wrong');

} else {
  var $mainFrame = $($main[0].contentDocument);

  // login page
  if ($mainFrame.find('div.title:contains("Login Facility Booking")').length > 0) {
    chrome.storage.sync.get({
      hkid: '',
      hkidDigit: '',
      dobDD: '',
      dobMM: '',
      dobYYYY: '',
    }, function(items) {
      $mainFrame.find('[name=patronFlag][value=N]').click();
      $mainFrame.find('#hkId').val(items.hkid);
      $mainFrame.find('#hkIdCheckDigit').val(items.hkidDigit);
      $mainFrame.find('#dobDD').val(items.dobDD);
      $mainFrame.find('#dobMM').val(items.dobMM);
      $mainFrame.find('#dobYYYY').val(items.dobYYYY);
      $mainFrame.find('.actionBtnContinue').click()
    });

  // details page
  } else if ($mainFrame.find('div.title > b:contains("Enter Booking Details")').length > 0) {

    $mainFrame.find('#searchBtnPanel input.actionBtn').trigger('click');

    var $date = $mainFrame.find('#datePanel select');
    $date.find('option:last').prop('selected', true);

    chrome.storage.sync.get({
      serverEndpoint: '',
      configProfile: '',
    }, function(items) {
      var url = items.serverEndpoint + '/config/' + items.configProfile + '/select.js';
      injectJqThenScript(url);
    });

  } else if ($mainFrame.find('div.title:contains("Booking Details")').length > 0) {
    chrome.storage.sync.get({
      name: '',
      phone: '',
      email: '',
    }, function(items) {
      $mainFrame.find('input[name=name]').val(items.name);
      $mainFrame.find('input[name=contactPhoneNo]').val(items.phone);
      $mainFrame.find('#emailAddress').val(items.email);
      $mainFrame.find('input[name="facilityDeclare.answer"]:first').click();
      $mainFrame.find('input.actionBtnContinue').trigger('click');
    });
  }
}

// to test in chrome developer tool
// $($('frame[name=main]')[0].contentDocument).find('input[name=name]')
