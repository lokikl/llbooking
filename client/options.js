// Saves options to chrome.storage
function save_options() {
  chrome.storage.sync.set({
    serverEndpoint: document.getElementById('serverEndpoint').value,
    configProfile: document.getElementById('configProfile').value,
    hkid: document.getElementById('hkid').value,
    hkidDigit: document.getElementById('hkidDigit').value,
    dobDD: document.getElementById('dobDD').value,
    dobMM: document.getElementById('dobMM').value,
    dobYYYY: document.getElementById('dobYYYY').value,
    name: document.getElementById('name').value,
    phone: document.getElementById('phone').value,
    email: document.getElementById('email').value,
  }, function() {
    // Update status to let user know options were saved.
    var status = document.getElementById('status');
    status.textContent = 'Options saved.';
    setTimeout(function() {
      status.textContent = '';
    }, 750);
  });
}

// Restores select box and checkbox state using the preferences
// stored in chrome.storage.
function restore_options() {
  // Use default value color = 'red' and likesColor = true.
  chrome.storage.sync.get({
    serverEndpoint: 'https://labpf.com:4002',
    configProfile: '',
    hkid: '',
    hkidDigit: '',
    dobDD: '',
    dobMM: '',
    dobYYYY: '',
    name: '',
    phone: '',
    email: '',
  }, function(items) {
    document.getElementById('serverEndpoint').value = items.serverEndpoint;
    document.getElementById('configProfile').value = items.configProfile;
    document.getElementById('hkid').value = items.hkid;
    document.getElementById('hkidDigit').value = items.hkidDigit;
    document.getElementById('dobDD').value = items.dobDD;
    document.getElementById('dobMM').value = items.dobMM;
    document.getElementById('dobYYYY').value = items.dobYYYY;
    document.getElementById('name').value = items.name;
    document.getElementById('phone').value = items.phone;
    document.getElementById('email').value = items.email;
  });
}
document.addEventListener('DOMContentLoaded', restore_options);
document.getElementById('save').addEventListener('click', save_options);
