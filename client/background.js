// Copyright (c) 2013 Loki. All rights reserved.

// check if 2 url are in same host
function is_same(url1, url2) {
  return url1.split('/').slice(0, 3).join() === url2.split('/').slice(0, 3).join();
}

function execute(file, tabId) {
  chrome.tabs.executeScript(tabId, {file:"json2.js"}, function() {
    chrome.tabs.executeScript(tabId, {file:"jquery-1.10.2.min.js"}, function() {
      chrome.tabs.executeScript(tabId, {file:file});
    });
  });
}

// Called when the user clicks on the browser action.
chrome.browserAction.onClicked.addListener(function(tab) {
  if (tab.title.indexOf('Leisure Link') == -1) {
    chrome.cookies.getAll({domain: "w2.leisurelink.lcsd.gov.hk"}, function(cookies) {
      cookies.forEach(function(c) {
        var url = c.secure ? 'https://' : 'http://';
        chrome.cookies.remove({url: url + c.domain + c.path, name: c.name});
        console.log('removed cookie ' + c.domain + ' ' + c.name);
      });
      var url = "http://w2.leisurelink.lcsd.gov.hk/leisurelink/application/checkCode.do?flowId=1&lang=EN";
      console.log('going to captcha page');
      chrome.tabs.update(tab.id, {url: url}, function(tab) {
        execute('inject.js', tab.id);
      });
    });
  } else {
    console.log('injecting');
    execute('inject.js');
  }
});

// chrome.tabs.onUpdated.addListener( function (tabId, changeInfo, tab) {
//   if (changeInfo.status == 'complete' && tab.active) {
//     if (tab.url.indexOf('checkCode.do') > -1 || tab.url.indexOf('leisurelink/humanTest') > -1) {
//       console.log('injecting in 1 second');
//       setTimeout(function() {
//         execute('inject.js');
//       }, 100);
//     }
//   }
// })
