#!/usr/bin/env bash

rm -rf output.png

# convert \
#   -tile challenge-bg.png \
#   -pointsize 24 \
#   label:'9xAyxe' \
#   -size 120x60 \
#   output.png

convert \
  -fill dodgerblue \
  -font Candice \
  -strokewidth 2 \
  -size 120x60 \
  -gravity center \
  label:'9xAyxe' \
  output.png

eog output.png
