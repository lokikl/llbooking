nnoremap sh :CtrlP server/views<cr>
nnoremap sa :tab drop server/server.rb<cr>

" Restart thin
" ask me for the definition of TmuxSendKey - loki
nnoremap <leader>r :call RestartLLServer()<cr>
function! RestartLLServer()
  call FocusMainTmuxSession()
  let p = getcwd()
  let cmd = " C-c ' cd " . p . "/server ; ./start.sh server' Enter"
  call TmuxSendKey("main", "llserver", cmd)
endfunction
